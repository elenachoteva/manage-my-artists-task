<?php

namespace App\Http\Controllers;

use App\Email;
use App\Events\AddedEmailEvent;
use App\Http\Traits\Validation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Validation;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendEmails(Request $request)
    {
        $this->isEmptyField($request);

        if ($this->validateSeparator($request->get('separator'))) {
            $emailsArray = $this->explodeEmailsTextField($request->get('emails'), $request->get('separator'));

            if (count($emailsArray) > 0) {
                $invalidEmails = [];
                $validEmails = [];

                foreach ($emailsArray as $emailRecord) {
                    $validator = Validator::make([
                        'uploader_ip' => request()->ip(),
                        'email' => trim($emailRecord)
                    ], Email::$storeRules);

                    if ($validator->fails()) {
                        $invalidEmails[] = trim($emailRecord);
                    } else {
                        $email = trim($emailRecord);

                        if (!in_array($email, $validEmails)) {
                            $validEmails[] = $email;

                            Email::create([
                                'uploader_ip' => request()->ip(),
                                'email' => trim($emailRecord)
                            ]);
                        }
                    }
                }

                if (count($validEmails) > 0) {
                    event(new AddedEmailEvent($validEmails));
                }

                return redirect()->route('homepage')
                    ->with([
                        'invalidEmails' => $invalidEmails,
                        'validEmails' => $validEmails,
                    ]);
            }

            return redirect()->route('homepage')->with('error', 'You must provide valid emails');
        }

        return redirect()->route('homepage')->with('error', 'You must provide valid separator');
    }

    /**
     * @param $request
     * @return \Illuminate\Http\RedirectResponse
     */
    private function isEmptyField($request)
    {
        if (!$request->has('emails')) {
            return redirect()->route('homepage')->with('error', 'The Emails field must be filled');
        }

        if (!$request->has('separator')) {
            return redirect()->route('homepage')->with('error', 'You must pick a separator');
        }
    }
}

<?php

namespace App\Http\Traits;

trait Validation
{
    private $separators = [
        "new_line" => "\r\n",
        "comma" => ",",
        "space" => " "
    ];

    /**
     * @param $separator
     * @return bool
     */
    private function validateSeparator($separator): bool
    {
        return in_array($separator, ['new_line', 'comma', 'space']);
    }

    /**
     * @param $text
     * @param $separator
     * @return array
     */
    private function explodeEmailsTextField($text, $separator): array
    {
        return explode($this->separators[$separator], $text);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'uploader_ip',
        'email'
    ];

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    public static $storeRules = [
        'uploader_ip' => 'required|ip|string|max:50',
        'email' => 'required|email|unique:emails'
    ];
}

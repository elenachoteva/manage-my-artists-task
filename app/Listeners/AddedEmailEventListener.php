<?php

namespace App\Listeners;

use App\Events\AddedEmailEvent;
use Illuminate\Support\Facades\Mail;
use App\Mail\AddedEmailMail;

class AddedEmailEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AddedEmailEvent $event
     */
    public function handle(AddedEmailEvent $event)
    {
        Mail::to($event->emails)->send(new AddedEmailMail());
    }
}

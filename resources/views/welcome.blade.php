<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Manage My Artists</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
          rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container d-flex justify-content-center align-content-center flex-column min-vh-100 my-5">
    <div class="w-100">
        <h1 class="display-4 mb-5">Submit your emails</h1>
        @if(session()->has('error'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session()->has('invalidEmails') && count(session()->get('invalidEmails')) > 0)
            <h5>Invalid emails:</h5>
            <ol>
                @forelse(session()->get('invalidEmails') as $email)
                    <li>{{ $email }}</li>
                @empty
                @endforelse
            </ol>
        @endif
        @if(session()->has('validEmails') && count(session()->get('validEmails')) > 0)
            <h5>Valid emails:</h5>
            <ol>
                @forelse(session()->get('validEmails') as $email)
                    <li>{{ $email }}</li>
                @empty
                @endforelse
            </ol>
        @endif
        <form method="post" id="email-form" action="{{ route('sendEmails') }}">
            @csrf
            <div class="mb-4">
                <label for="separatorSelect">Separator</label>
                <select id="separatorSelect" class="@error('separator') is-invalid @enderror form-control" name="separator">
                    <option value="">Pick separator</option>
                    <option value="new_line">New line</option>
                    <option value="comma">Comma</option>
                    <option value="space">Space</option>
                </select>
                @error('separator')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-4">
                <label for="emailsTextarea">Emails</label>
                <textarea name="emails" class="@error('emails') is-invalid @enderror form-control" id="emailsTextarea" placeholder="Paste emails" rows="10" required></textarea>
                @error('emails')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="d-flex">
                <button class="btn btn-primary ml-auto" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
